# -*- coding: utf-8 -*-
"""
Created on Thu Jan 08 14:29:33 2015

@author: Andy
"""

# TODO: finish with http://flask.pocoo.org/docs/0.10/quickstart/

from flask import Flask, render_template, url_for
app = Flask(__name__)

@app.route('/')
def index():
    return 'Index Page'

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        #do_the_login()
        pass
    else:
        #show_the_login_form()
        pass
    
@app.route('/hello')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name)

@app.route('/user/<username>')
def profile(username):
    # show the user profile for that user
    return 'User %s' % username
    
@app.route('/post/<int:post_id>')
def show_post(post_id):
    # show the post with the given id, the id is an integer
    return 'Post %d' % post_id

if __name__ == '__main__':
    #app.debug = True
    app.run()
    #app.run(host='0.0.0.0')
    with app.test_request_context():
        print url_for('index')
        print url_for('login')
        print url_for('login', next='/')
        print url_for('profile', username='John Doe')